function KBCBListener(config){
	if(!config) config = {};
  	this._cbm = config.checkBufferLength || 20;
	this._ci = config.caseInsensitive || true;
	this._cc = function(e){
		let z = this;
		if(!e.key || e.key == "Tab" || e.key == "Shift" || e.key == "Alt" || e.key == "Control" || e.key == "Escape"  || e.key.match(/F\d+/)) return;
		z._t+=e.key;
		if(z._t.length > z._cbm) z._t = z._t.substring(z._t.length-z._cbm);
		if(z._a && z._f){
			let b = z._t.match(z._a);
			if(b){
				z._f(b[0], z._t);
				z._t = "";
			}			
		}
		if(z._s){
			for(let i=0; i<z._s.length; i++){
				let k = z._s[i].keyword;
				let t = z._t;
				if(z._ci){
					k = k.toUpperCase();
					t = t.toUpperCase();
				}
				if(t.match(k)){
					z._s[i].callback(z._t.substring(t.indexOf(k), z._t.length), z._t);
					z._t = "";
				}
			}			
		}
	};
	this.setRegexCb = function(r, f){
		this._a = r;
		this._f = f;
	};
	this.setCbList = function(a){
		this._s = a;
	}
	this._t = "";
	this._a;
	this._f;
	this._s;
	var m = this;
	document.addEventListener('keyup', function(a){m._cc(a, m)}, false);
	return m;
}